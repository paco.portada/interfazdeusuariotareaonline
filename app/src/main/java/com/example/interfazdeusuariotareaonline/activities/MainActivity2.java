package com.example.interfazdeusuariotareaonline.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.interfazdeusuariotareaonline.Constants_ES;
import com.example.interfazdeusuariotareaonline.R;
import com.example.interfazdeusuariotareaonline.Utils;

import java.util.InputMismatchException;


public class MainActivity2 extends AppCompatActivity {

    Utils utils = new Utils();
    static TextView tv1;
    static TextView tv2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

    }

    public void onRadioButtonFromDollarToEuroClicked(View view) {

        boolean checked = ((RadioButton) view).isChecked();
        tv1 = findViewById(R.id.editTextInputDolar);
        tv2 = findViewById(R.id.editTextInputEuro);


        try {
            if (checked) {
                tv1.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }
                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        try {
                            if (tv1.hasFocus()) {
                                utils.setDolarAmount(Double.parseDouble(tv1.getText().toString()));
                                tv2.setText(utils.fromDolarToEuro());
                            }
                        } catch (NumberFormatException nfe) {
                            nfe.printStackTrace();
                        }
                    }
                    @Override
                    public void afterTextChanged(Editable s) {
                    }
                });
            }

        } catch (Exception nfe) {
            Toast.makeText(this, Constants_ES.MESSAGE_ERROR_FOR_EURO, Toast.LENGTH_SHORT).show();
        }

    }

    public void onRadioButtonFromEuroToDollarClicked(View view) {

        boolean checked = ((RadioButton) view).isChecked();
        tv1 =findViewById(R.id.editTextInputDolar);
        tv2 =findViewById(R.id.editTextInputEuro);

        try {
            if (checked) {
                tv2.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }
                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                        try {
                            if (tv2.hasFocus()) {
                                utils.setEuroAmount((Double.parseDouble(tv2.getText().toString())));
                                tv1.setText(utils.fromEuroToDollar());
                            }
                        } catch (NumberFormatException nfe) {
                            nfe.printStackTrace();
                        }
                    }
                    @Override
                    public void afterTextChanged(Editable s) {
                    }
                });
            }
        } catch (Exception nfe) {
            Toast.makeText(this, Constants_ES.MESSAGE_ERROR_FOR_EURO, Toast.LENGTH_SHORT).show();
        }

    }

}





