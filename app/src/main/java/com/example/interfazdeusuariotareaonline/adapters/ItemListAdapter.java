package com.example.interfazdeusuariotareaonline.adapters;

/**
 * @author Rnarvaiza
 */

/**
 * On this adapter we keep under comment (//)  two lines to enable/disable the Libro1 class constructor.
 * So if you are going to test this app under AVD manager, you must uncomment line 27 and comment line 26. Then
 * you have to replace all Libro references for Libro1.
 * Also you have to uncomment line 58 to enable holder and comment line 59 to disable Glide.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.example.interfazdeusuariotareaonline.R;
import com.example.interfazdeusuariotareaonline.models.Libro;
//import com.example.interfazdeusuariotareaonline.models.Libro1;

import java.util.ArrayList;

public class ItemListAdapter extends RecyclerView.Adapter<ItemListAdapter.ListViewHolder> {
    private Context context;
    private ArrayList<Libro> listLibros;

    public ItemListAdapter(Context context) {
        this.context = context;
    }

    public ArrayList<Libro> getListLibros() {
        return listLibros;
    }

    public void setListLibros(ArrayList<Libro> listLibros) {
        this.listLibros = listLibros;
    }

    @NonNull
    @Override
    public ListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemList = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, parent, false);
        return new ListViewHolder(itemList);
    }

    @Override
    public void onBindViewHolder(@NonNull ListViewHolder holder, int position) {

        holder.tvTitulo.setText(getListLibros().get(position).getTitulo());
        //holder.imgList.setImageResource(listLibros.get(position).getFoto());
        Glide.with(context).load(getListLibros().get(position).getFoto()).into(holder.imgList);
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, (getListLibros().get(position).getAutor() + "\n" + getListLibros().get(position).getFechaPublicacion()), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return getListLibros().size();
    }

    public class ListViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitulo;
        ImageView imgList;
        RelativeLayout relativeLayout;


        public ListViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTitulo = itemView.findViewById(R.id.tituloLibro);
            imgList = itemView.findViewById(R.id.img_list);
            relativeLayout = itemView.findViewById(R.id.relative_layout);
        }
    }
}
