package com.example.interfazdeusuariotareaonline;

import com.example.interfazdeusuariotareaonline.activities.MainActivity2;

public class Constants_ES extends MainActivity2 {

    public static final String MESSAGE_ERROR_FOR_DOLLAR = ("No es posible convertir la linea de caracteres 'Dólares' ya que no es un valor numérico");
    public static final String MESSAGE_ERROR_FOR_EURO = ("No es posible convertir la linea de caracteres 'Euros' ya que no es un valor numérico");

}
