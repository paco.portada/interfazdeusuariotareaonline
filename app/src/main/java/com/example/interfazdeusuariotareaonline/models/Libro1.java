package com.example.interfazdeusuariotareaonline.models;

/**
 * @author Rnarvaiza
 */

/**
 * This method is designed only for test on Android studio AVD Manager.
 * After several hours trying to get access for url files and checking that is not able to.
 * Owing to this circunstances, I had to modify AndroidManifest to allow the app get connected to internet.
 * The unique diference between Libro class constructor and Libro1 class constructor is that Libro1 has an int parameter to get access to the R.drawable.ID.
 * With this ID, then I build throw parameters the Arraylist of objects Libro1.
 */

import android.os.Parcel;
import android.os.Parcelable;

public class Libro1 implements Parcelable {

    public Libro1(String titulo, int foto, String autor, String fechaPublicacion) {
        this.titulo = titulo;
        this.autor = autor;
        this.fechaPublicacion = fechaPublicacion;
        this.foto = foto;
    }


    private String titulo;
    private String autor;
    private String fechaPublicacion;
    private int foto;

    public int getFoto() {
        return foto;
    }

    public void setFoto(int foto) {
        this.foto = foto;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getFechaPublicacion() {
        return fechaPublicacion;
    }

    public void setFechaPublicacion(String fechaPublicacion) {
        this.fechaPublicacion = fechaPublicacion;
    }

    public Libro1() {
    }

    protected Libro1(Parcel in) {
        titulo = in.readString();
        autor = in.readString();
        fechaPublicacion = in.readString();
        foto = in.readInt();
    }

    public static final Creator<Libro> CREATOR = new Creator<Libro>() {
        @Override
        public Libro createFromParcel(Parcel in) {
            return new Libro(in);
        }

        @Override
        public Libro[] newArray(int size) {
            return new Libro[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(titulo);
        dest.writeString(autor);
        dest.writeString(fechaPublicacion);
        dest.writeInt(foto);
    }
}
