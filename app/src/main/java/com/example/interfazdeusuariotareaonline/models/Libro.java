package com.example.interfazdeusuariotareaonline.models;

/**
 * @author Rnarvaiza
 */

import android.os.Parcel;
import android.os.Parcelable;

public class Libro implements Parcelable {

    public Libro(String titulo, String autor, String editorial, String fechaPublicacion, String sinopsis, String foto) {
        this.titulo = titulo;
        this.autor = autor;
        this.editorial = editorial;
        this.fechaPublicacion = fechaPublicacion;
        this.sinopsis = sinopsis;
        this.foto = foto;
    }

    private String titulo, autor, editorial, fechaPublicacion, sinopsis, foto;

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getEditorial() {
        return editorial;
    }

    public void setEditorial(String editorial) {
        this.editorial = editorial;
    }

    public String getFechaPublicacion() {
        return fechaPublicacion;
    }

    public void setFechaPublicacion(String fechaPublicacion) {
        this.fechaPublicacion = fechaPublicacion;
    }

    public String getSinopsis() {
        return sinopsis;
    }

    public void setSinopsis(String sinopsis) {
        this.sinopsis = sinopsis;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public Libro() {
    }

    protected Libro(Parcel in) {
        titulo = in.readString();
        autor = in.readString();
        editorial = in.readString();
        fechaPublicacion = in.readString();
        sinopsis = in.readString();
        foto = in.readString();
    }

    public static final Creator<Libro> CREATOR = new Creator<Libro>() {
        @Override
        public Libro createFromParcel(Parcel in) {
            return new Libro(in);
        }

        @Override
        public Libro[] newArray(int size) {
            return new Libro[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(titulo);
        dest.writeString(autor);
        dest.writeString(editorial);
        dest.writeString(fechaPublicacion);
        dest.writeString(sinopsis);
        dest.writeString(foto);
    }
}
